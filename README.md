# front-end case

## Before you start

   For this case, we would like you to use the open data Flight Status API.

   To get access and use this API, plz start by creating an account in the Airfrance KLM developer portal:

* Go to https://developer.airfranceklm.com/
* Go for "sign in / register"
* Go for "sign up"
* Fill in the details required and make sure you **check the box** "Issue a new key for LE Open Data API" !!

After confirming your registration (via the confirmation link in the email you received) you are ready to use the Flight Status API.
 
## To keep in mind
You should use the API Key in the "api-key" header of your requests to the Flight Status API along with the "accept" header.

More Flight Status API documentation can be found here: https://developer.airfranceklm.com/docs/read/opendata/flight_status_api
 
## Case instructions

### Functional instructions

* Using the flight status API, create a page that shows the status of the flights that are scheduled from now until tomorrow.
* The status of the flight should be easily recognizable (on time, delayed, cancelled, etc.).
* Add functionalities to make your application user friendly (i.e. a search on flight number).

### Time box

* Please spend three to max four hours on this assignment.

### Desired technologies

* Angular 7+ or other modern data-binding framework. 
* The use of Angular Material is allowed.
 
### Bonus points
* Make it responsive.

### Delivery
Please share your project preferably via your own bitbucket repository.
 
 
### Known bugs
* Search bar is not working properly
* As the Ionic alert modal can't handle Promises, sometimes the refresh list doesn't work :(


### Future enhancements
* Find a proper API to get nearest airports
* Search bar is able to filter by status, flight prefix & etc
* Use Redux or Akita
* Be able to mark a flight as favorite in order to follow it
* Add companies logos