import { Component } from '@angular/core';
import { StorageService } from '../services/storage/storage.service';
import { AirportsFinderService } from '../services/airportfinder/airportsfinder.service';
import { AlertService } from '../services/alertservice/alert.service';
import { AirportsScheduleService } from '../services/airportschedule/airportschedule.service';
import { async } from '@angular/core/testing';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  public airportList: any = [];
  public airport: any = {};
  public loading: boolean;
  public flightList: any = [];
  public filteredFlightList: any = [];
  public displaySearchBar: boolean = false;

  constructor(
    private storageService: StorageService,
    private airportFinderService: AirportsFinderService,
    private alertService: AlertService,
    private airportScheduleService: AirportsScheduleService,
    public events: Events,
  ) {
    this.events.subscribe('load:flights', () =>  {
      this.fetchData();
    });
  }

  ionViewDidEnter() {
    this.initLocationAndAirportList()
      .then(airportList => {
        this.airportList = !!airportList && airportList.data.airports ? 
        airportList.data.airports.map((airport: { name: any; code: any; countryCode: any; }) => {
          return {
            name: airport.name,
            type: 'radio',
            label: `${airport.code} - ${airport.name} ${airport.countryCode}`,
            value: airport
          };
        }) : [];

        return this.storageService.get('selectedAirport');
      })
      .then(data => {
        this.loading = true;
        if (data) {
          this.airport = data;
          this.events.publish('load:flights');
        } else {
          this.chooseAirport();
        }
      })

      .catch(error => {})
  }

  fetchData(): Promise<any> {
    this.loading = true;
    return this.airportScheduleService.getScheduledFlightsFrom(this.airport.code)
      .then(result => {
        this.flightList = result.operationalFlights;
        this.filteredFlightList = this.flightList;

        this.loading = false;
      })
      .catch(() => {
        this.loading = false;
      })
  }

  getBadgeColor(status: string): string {
    switch (status) {
      case 'Arrived':
        return 'success';

      case 'Cancelled':
        return 'danger';

      default:
        return 'primary';
    }
  }

  ionViewDidLeave() {
    this.events.unsubscribe('load:flights');
  }

  showSearchBar() {
    this.displaySearchBar = !this.displaySearchBar;
  }

  chooseAirport() {
    this.alertService.showSelectAirportAlert(this.airportList, (data) => {
      this.airport = data;
      this.storageService.set('selectedAirport', data);
      this.events.publish('load:flights');
    })
  }

  searchFlights(event: any) {
    this.filteredFlightList = this.flightList.filter(flight => {
      const flightStatus = flight.flightStatusPublicLangTransl;

      return !!flightStatus && flightStatus.includes(event.detail.value.toLocaleLowerCase());
    })
  }

  private async initLocationAndAirportList(): Promise<any> {
    // const coords = await this.geolocationService.getCurrentPosition();
    return this.airportFinderService.getNearbyAirports('', '');
  }

}
